/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_re_int_tab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/22 09:30:22 by antgalan          #+#    #+#             */
/*   Updated: 2022/10/22 09:33:01 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_rev_int_tab(int *tab, int size)
{
	int	aux;
	int	i;
	int	m;

	i = 0;
	while (i < size / 2)
	{
		aux = tab[i];
		m = size - i -1;
		tab[i] = tab[m];
		tab[m] = aux;
		i++;
	}
}
